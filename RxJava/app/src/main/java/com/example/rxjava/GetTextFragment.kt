package com.example.rxjava

import android.os.Bundle
import android.util.JsonReader
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.rxjava.databinding.FragmentGetTextBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.core.Observable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.FileReader
import java.io.IOException
import java.lang.reflect.Type
import java.nio.charset.Charset
import kotlin.random.Random


class GetTextFragment : Fragment() {
    private val BASE_URL = "https://jsonplaceholder.typicode.com/"
    private lateinit var binding: FragmentGetTextBinding
    private val TAG = GetTextFragment::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGetTextBinding.inflate(inflater)

        binding.btNav2.selectedItemId = R.id.next
        binding.btNav2.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.next -> {Navigation.findNavController(binding.root).navigate(R.id.nav_getTextFrag_to_showTextFrag)}
            }
            true
        }

        binding.btRest.setOnClickListener {
            try{
                val retrofitBuilder = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build()
                    .create(ApiInterface::class.java)
                CoroutineScope(Dispatchers.IO).launch {
                    retrofitData = retrofitBuilder.getData()
                    binding.tvSuccess.text = "Success get data with RestAPI"
                    Log.e(TAG, "Success get data with RestAPI")
                }
                isRest = true
                isJson = false
            } catch(e: Exception){
                binding.tvSuccess.text = e.localizedMessage
                Log.e(TAG, "${e.localizedMessage}")
            }
        }

        binding.btJson.setOnClickListener {
            try {
                jsonData =
                    Gson().fromJson(getJSONFromAssets(), MyJSONData::class.java)
                isRest = false
                isJson = true
                binding.tvSuccess.text = "Success get data with JSON"
                Log.e(TAG, "Success get data with JSON")

            } catch (e: Exception){
                binding.tvSuccess.text = e.localizedMessage
                Log.e(TAG, "${e.localizedMessage}")
            }
        }
        return binding.root
    }

    private fun getJSONFromAssets(): String? {

        var json: String? = null
        val charset: Charset = Charsets.UTF_8
        try {
            val myUsersJSONFile = requireActivity().assets.open("myjson.json")
            val size = myUsersJSONFile.available()
            val buffer = ByteArray(size)
            myUsersJSONFile.read(buffer)
            myUsersJSONFile.close()
            json = String(buffer, charset)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }
    companion object {
        @JvmStatic
        fun newInstance() =  GetTextFragment()
        lateinit var retrofitData:List<MyDataItem>
        lateinit var jsonData:List<MyJSONDataItem>
        var isRest = false
        var isJson = false
        fun dataRestSource(): Observable<Int> {
            return Observable.create{ subscriber ->
                for(i in 0..retrofitData.size){
                    Thread.sleep(500)
                    subscriber.onNext(i)
                }
            }
        }
        fun dataJSONSource(): Observable<Int> {
            return Observable.create{ subscriber ->
                subscriber.onNext(Random.nextInt(0, jsonData.size))
            }
        }
    }
}