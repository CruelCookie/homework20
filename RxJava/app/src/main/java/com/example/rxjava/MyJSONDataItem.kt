package com.example.rxjava

data class MyJSONDataItem(
    val id: Int,
    val text: String
)