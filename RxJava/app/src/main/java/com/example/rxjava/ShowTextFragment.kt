package com.example.rxjava

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.rxjava.databinding.FragmentShowTextBinding
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class ShowTextFragment : Fragment() {
    private lateinit var binding: FragmentShowTextBinding
    private val TAG = ShowTextFragment::class.java.simpleName

    companion object{
        @JvmStatic
        fun newInstance() = ShowTextFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentShowTextBinding.inflate(inflater)

        binding.btNav.selectedItemId = R.id.next
        binding.btNav.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.next -> {Navigation.findNavController(binding.root).navigate(R.id.nav_showTextFrag_to_getTextFrag)}
            }
            true
        }

        binding.btStart.setOnClickListener {
            if(GetTextFragment.isRest){
                val dispose = GetTextFragment.dataRestSource()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                    binding.tvText.text = "${GetTextFragment.retrofitData[it]}"
                    Log.e(TAG, "next int $it")
                },{
                    Toast.makeText(context,"All texts were shown or you didn't click any button on first fragment", Toast.LENGTH_SHORT).show()
                    binding.tvText.text = "${it.localizedMessage}"
                    Log.e(TAG, "it ${it.localizedMessage}")
                })
            } else if(GetTextFragment.isJson){
                val dispose = GetTextFragment.dataJSONSource()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        Toast.makeText(context,"Text from JSON is shown", Toast.LENGTH_SHORT).show()
                        binding.tvText.text = "${GetTextFragment.jsonData[it]}"
                        Log.e(TAG, "random int $it")
                    },{
                        Toast.makeText(context,"You didn't click any button on first fragment or I don't know what", Toast.LENGTH_SHORT).show()
                        binding.tvText.text = "${it.localizedMessage}"
                        Log.e(TAG, "it ${it.localizedMessage}")
                    })
            }
        }

        return binding.root
    }
}