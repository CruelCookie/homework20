package com.example.rxjava

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.navigation.Navigation
import com.example.rxjava.databinding.ActivityMainBinding
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers


class MainActivity : AppCompatActivity() {
    private val TAG = MainActivity::class.java.simpleName
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
//
//        val observable = Observable.just(1, 2, 3)
//
//        val dispose = observable.subscribe({
//            Log.e(TAG, "New data $it")
//        })
//
//
//        binding.btTest.setOnClickListener {
//            Log.e(TAG, "click click")
//        }
//
//        val dispose = dataSource()
//            .subscribeOn(Schedulers.newThread())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe({
//            binding.btTest.text = "Next int $it"
//            Log.e(TAG, "next int $it")
//        },{
//            Log.e(TAG, "it ${it.localizedMessage}")
//        })
    }

}